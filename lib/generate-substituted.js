function pad(num, size) {
    const s = "0000000000" + num;
    return s.substr(s.length - size);
}

/**
 * Accepts a collection of words in a one dimensional dictionary form and replaces configured segments to generate a substitution dictionary.
 * @param     {String[]}    dictInput    Source word collection
 * @return    {String[]}                 Generated substitution word collection
 */
function generateSubstituted(dictInput) {
	// Determine required parameters from configuration.
	const config = require("../config");
	const pre = config.processing.preflight;
	const rep = config.processing.replacement;
	const step = Math.floor(dictInput.length / 50);
	// Preform regular expressions as instances.
	const patternPreflight = new RegExp(pre.pattern);
	const patternsReplacement = [];
	for(let instruction of rep) {
		patternsReplacement.push({
			"pattern": new RegExp(instruction.character),
			"subtitute": instruction.substitute
		});
	}
	// Move through supplied dictionary and replace by instructions.
	const dictOutput = [];
	for(let lineIndex = 0, lineLength = dictInput.length; lineIndex < lineLength; lineIndex++) {
		if(lineIndex % step === 0) {
			console.log(`Processing dictionary, at line ${
				pad(lineIndex, lineLength.toString().length)
			}/${
				lineLength
			}, completed ${
				Math.round((lineIndex / lineLength) * 100)
			}%.`);
		}
		// Prepare current dictionary line.
		const original = dictInput[lineIndex];
		// Execute preflight on current line to exclude empty promises.
		if(patternPreflight.test(original) === pre.outcome) { continue; }
		let substituted = original;
		// Process all instructions from patterns.
		for(let instruction of patternsReplacement) {
			substituted = substituted.replace(instruction.pattern, instruction.subtitute);
		}
		dictOutput.push({original, substituted});
	}
	return dictOutput;
}

module.exports = generateSubstituted;
