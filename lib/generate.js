/* _____  _    _ _   _  _____ _    _ _      _____ _   _ ______
 |  __ \| |  | | \ | |/ ____| |  | | |    |_   _| \ | |  ____|
 | |__) | |  | |  \| | |    | |__| | |      | | |  \| | |__
 |  ___/| |  | | . ` | |    |  __  | |      | | | . ` |  __|
 | |    | |__| | |\  | |____| |  | | |____ _| |_| |\  | |____
 |_|     \____/|_| \_|\_____|_|  |_|______|_____|_| \_|______|
*/

// Initialise application with native modules and welcome message.
const path = require("path");
const fs = require("fs");

const generateSubstituted = require("./generate-substituted");
const generatePropertyList = require("./generate-plist");

// Load local paths and configuration.
const pathLocal = path.join();
const configPackage = require("../package.json");
const config = require("../config");

console.log(`Initialising Punchline Substitute Generator for macOS (v${configPackage.version} by August S. Freytag).`);

// Determine and log path to source file (dictionary to be parsed).
const sourceFilePath = path.join(pathLocal, "material/", config.input.path);
console.log(`Source file path determined: '${sourceFilePath}'.`);

// Get handle on source file and read it into memory.
const sourceFileStats = fs.statSync(sourceFilePath);
const sourceFileContents = fs.readFileSync(sourceFilePath).toString();
const sourceDictionary = sourceFileContents.split("\n");
console.log(`Read source file and stored in memory (${sourceFileStats.size / 1000} KB).`);

const outputCollection = generateSubstituted(sourceDictionary);
console.log(`Received output dictionary with ${outputCollection.length} line(s).`);

const outputPropertyList = generatePropertyList(outputCollection);
console.log(`Generated property list, writing to output path.`);

const outputFilePath = path.join(pathLocal, "material/", config.output.path);
fs.writeFileSync(outputFilePath, outputPropertyList);

console.log(`Property list written to '${config.output.path}'.`);
