function generatePropertyList(dictOutput) {
	const config = require("../config");
	// Truncate all but a few entries of output dictionary if configuration indicates a demo run.
	if(config.output.demonstrate) {
		dictOutput = dictOutput.splice(0, 5);
	}
	// Define headers, footers and subsequently the body of the list.
	const plistHeaders = `<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">\n<plist version="1.0">\n<array>`;
	const plistFooters = `</array>\n</plist>`;
	const plistBodyBuffer = dictOutput.reduce(function(buffer, set) {
		const segmentPhrase = `<key>phrase</key>\n<string>${set.original}</string>`;
		const segmentShortcut = `<key>shortcut</key>\n<string>${set.substituted}</string>`;
		buffer.push(`<dict>\n${segmentPhrase}\n${segmentShortcut}\n</dict>`);
		return buffer;
	}, []);
	return `${plistHeaders}\n${plistBodyBuffer.join("\n")}\n${plistFooters}`;
}

module.exports = generatePropertyList;
